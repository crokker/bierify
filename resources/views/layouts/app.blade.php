<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Bierify</title>

    <!-- Styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">





    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>

    @yield('content');

    <!-- Scripts -->
    <script src="/js/api.js"></script>
    <script src="/js/app.js"></script>

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>

    <script src="/js/main.js"></script>

    <script>
        $(document).ready( function(){
            var beercounter;
            $('.addbeer').click( function(){
                $('.beertotal').append('<img src="img/pintblack.svg" class="beertotal-beer"/>');
                api.addDrink(1);
                api.getUser(function(data){
                    console.log(data);
                })
            });
            $('#outofbeer').one ('click', function(){
            })
            $('#returnbeer').one ('click', function(){
                api.addReturn()
                $('#message').append('Top, Lekker bezig');
            })

            $('#getbeercontainer').hide();
            // api.getBeerQuotas(function(data){
            //     console.log(data);
            //     for (var i = 0; i < data.length; i++){
            //         if (data[i].need_purchase){
            //             $('#container').addClass('.blur');
            //             $('#getbeercontainer').show();
            //             $('#crate').click( function(){
            //                 api.addDrink(24);
            //                 $('#getbeercontainer').hide();
            //
            //             })
            //             $('#twelvepack').click( function(){
            //                 api.addDrink(12);
            //                 $('#getbeercontainer').hide();
            //
            //             })
            //             $('#sixpack').click( function(){
            //                 api.addDrink(6);
            //                 $('#getbeercontainer').hide();
            //             })
            //         }
            //     }
            // });

            api.getBeerQuotas(function(data){
                for (var i = 0; i < data.length; i++){
                    console.log(data[i]);
                    $('#chart').append('<div class="chartbar" style="height: ' + data[i].beer_quota + '%;"><div class="chartbar-img" style ="background-image: url("img/dennis.jpg")">' + data[i].name + '</div></div>');
                }
            })


            api.getUser( function(userData){
                api.getStock(function(stock){
                    if (stock < 7){
                        api.getBuyer( function(buyerData){
                            if(buyerData.id == userData.id){
                                $('#container').addClass('blur');
                                $('#getbeercontainer').show();
                                $('#crate').click( function(){
                                    api.addPurchase(24);
                                    $('#getbeercontainer').hide();
                                    $('#container').removeClass('blur');


                                })
                                $('#twelvepack').click( function(){
                                    api.addPurchase(12);
                                    $('#getbeercontainer').hide();
                                    $('#container').removeClass('blur');


                                })
                                $('#sixpack').click( function(){
                                    api.addPurchase(6);
                                    $('#getbeercontainer').hide();
                                    $('#container').removeClass('blur');

                                })

                            } else {
                                $('.getsbeer').show();
                            }
                        });

                    }
                });
            });


            $('.getsbeer').hide();



        });

    // api.getUser(function(data){
    //     console.log(data);
    // })
    </script>

</body>
</html>
