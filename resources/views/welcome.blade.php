@extends('layouts.app')

@section('content')

<div id="getbeercontainer" style="display: none;">
    <div id="blur"></div>
    <div id="getbeer">
        <h2>Kut man, je moet bier halen</h2>
        <br>
        <p>Ik heb gehaald:<p>
            <br>
        <img id="crate" src="img/krat.jpg"/>
        <img id="twelvepack" src="img/twelvepack.png"/>
        <img id="sixpack" src="img/sixpack.jpg"/>
        <br><br>
    </div>
</div>
<div id="container">
    <header>
        <img id="logo" src="img/bierify.png"/>
        <div class="statusitem bierkoning">
            <div class="statusitem-img"></div>
            <p>Bierkoning</p>
        </div>
        <div class="statusitem weggebracht">
            <div class="statusitem-img"></div>
            <p>Laatst weggebracht</p>
        </div>
        <div class="statusitem gekocht">
            <div class="statusitem-img"></div>
            <p>Laatst gekocht</p>
        </div>
    </header>
    <div class="beermanager">
        <div class="addbeer">
            +<img src="img/pint.svg"/>
        </div>
        <div class="beertotal"></div>
    </div>

    <div class="buttoncontainer">
        {{-- <div id="outofbeer" class="button ">Het bier is (bijna) op</div> --}}
        <div id="returnbeer" onclick="" class="button ">Ik ga bier terugbrengen</div>
    </div>
    <div id="message"></div>


    <div class="getsbeer">
        <div class="getsbeer-img" style ='background-image: url("img/dennis.jpg")'></div>
        <span class="name">MOET BIER HALEN
    </div>
    {{-- <canvas id="myChart" width="400" height="400"></canvas> --}}



    <footer>
        <div id="chart">

        </div>
    </footer>
</div>
@endsection
