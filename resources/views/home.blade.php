@extends('layouts.app')

@section('content')

<div id="getbeercontainer">
    <div id="blur"></div>
    <div id="getbeer">
        <h2>Kut man, je moet bier halen</h2>
        <p>Ik haal<p>
        <form>
        <select >
        @for ($i = 0; $i < 6; $i++)
            <option>{{$i}}</option>
        @endfor
        </select>

    </div>
</div>
    <header>
        <img id="logo" src="img/bierify.png"/>
        <div class="statusitem">
            <div class="statusitem-img" style ='background-image: url("img/dennis.jpg")'></div>
            <p>Bierkoning</p>
        </div>
        <div class="statusitem">
            <div class="statusitem-img" style ='background-image: url("img/dennis.jpg")'></div>
            <p>Laatst weggebracht</p>
        </div>
        <div class="statusitem">
            <div class="statusitem-img" style ='background-image: url("img/dennis.jpg")'></div>
            <p>Laatst gekocht</p>
        </div>
    </header>
    <div class="beermanager">
        <div class="addbeer">
            +<img src="img/pint.svg"/>
        </div>
        <div class="beertotal"></div>
    </div>

    <div class="buttoncontainer">
        <div id="outofbeer" class="button ">Het bier is (bijna) op</div>
        <div id="returnbeer" onclick="" class="button ">Ik ga bier terugbrengen</div>
    </div>

    <div class="getsbeer">
        <div class="getsbeer-img" style ='background-image: url("img/dennis.jpg")'></div>
        MOET BIER HALEN
    </div>
    {{-- <canvas id="myChart" width="400" height="400"></canvas> --}}



    <footer>
        <div id="chart">
            <div class="chartbar" style="height: 100%;">
                <div class="chartbar-img" style ='background-image: url("img/dennis.jpg")'></div>
            </div>
            <div class="chartbar" style="height: 100%;">
                <div class="chartbar-img" style ='background-image: url("img/dennis.jpg")'></div>
            </div>
            <div class="chartbar" style="height: 100%;">
                <div class="chartbar-img" style ='background-image: url("img/dennis.jpg")'></div>
            </div>
        </div>
    </footer>
@endsection
