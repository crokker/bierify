<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware' => 'auth'], function () {
	Route::get('/', function () {
	    return view('welcome');
	});

	Route::get('/logout', 'Auth\LoginController@logout');

	Route::get('/home', 'HomeController@index');
	Route::group(['prefix' => 'api'], function () {
		Route::get('/addDrink/{amount}', 'ApiController@addDrink');
		Route::get('/addPurchase/{amount}', 'ApiController@addPurchase');
		Route::get('/addReturn', 'ApiController@addReturn');
		Route::get('/getUser', 'ApiController@getUser');
		Route::get('/getBeerQuotas', 'ApiController@getBeerQuotas');
		Route::get('/getLatestLogs', 'ApiController@getLatestLogs');
		Route::get('/getStock', 'ApiController@getStock');
		Route::get('/getBuyer', 'ApiController@getBuyer');
	});
});
Auth::routes();
