var api = {
	getUser: function( fn ){
		if( typeof fn !== 'function' ){
			console.error('Moet functie zijn');
			return false;
		}
		$.get('/api/getUser', function(data){
			fn( data );
		});
	},
	getLatestLogs: function( fn ){
		if( typeof fn !== 'function' ){
			console.error('Moet functie zijn');
			return false;
		}
		$.get('/api/getLatestLogs', function(data){
			fn( data );
		});
	},
	getBeerQuotas: function( fn ){
		if( typeof fn !== 'function' ){
			console.error('Moet functie zijn');
			return false;
		}
		$.get('/api/getBeerQuotas', function(data){
			fn( data );
		});
	},
	getStock: function( fn ){
		if( typeof fn !== 'function' ){
			console.error('Moet functie zijn');
			return false;
		}
		$.get('/api/getStock', function(data){
			fn( data );
		});
	},
	getBuyer: function( fn ){
		if( typeof fn !== 'function' ){
			console.error('Moet functie zijn');
			return false;
		}
		$.get('/api/getBuyer', function(data){
			fn( data );
		});
	},
	addDrink: function(amount){
		$.get('/api/addDrink/'+amount);
	},
	addPurchase: function(amount){
		$.get('/api/addPurchase/'+amount);
	},
	addReturn: function(){
		$.get('/api/addReturn');
	},
}