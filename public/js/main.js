$(document).ready(function(){
	var logs;
	api.getLatestLogs(function(d){
		logs = d;
		$('.bierkoning .statusitem-img').html(logs.beerking.name.toUpperCase().substring(0,3));
		$('.weggebracht .statusitem-img').html(logs.returner.user.name.toUpperCase().substring(0,3));
		$('.gekocht .statusitem-img').html(logs.purchaser.user.name.toUpperCase().substring(0,3));
	});
});