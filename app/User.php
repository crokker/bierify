<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use App\Log;
use App\Kas;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'total_today_last'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function addDrink(  $amount ){
        Kas::substract($amount);
        $this->total_drinks += $amount;
        $this->beer_points += $amount;
        if( $this->isNewPeriod($this->total_today_last) ){
            $this->total_today = 0;
            $this->total_today_last = Carbon::now();
        }
        $this->total_today += $amount;
        $this->save();
    }

    public function getTotalTodayAttribute( $value ){
        if( $this->isNewPeriod($this->total_today_last) ){
            $value = 0;
            $this->total_today_last = Carbon::now();
            $this->save();
        }
        return $value;
    }

    public function addPurchase($amount){
        $this->generateNewBeerPoints($amount);
        Kas::add($amount);
        Log::create([
            'user_id' => $this->id,
            'type' => 'purchased',
        ]);
    }

    private function generateNewBeerPoints($amount){
        $beer_quota = $this->getBeerQuota();
        if($beer_quota == 0)
            return;
        $new_points = 100 / $beer_quota * $amount; 
        $this->beer_points -= $new_points;
        if( $this->beer_points < 0 )
            $this->beer_points = 0;
        $this->save();
    }

    public function getBeerQuota(){
        $total_beer_points = Self::get()->sum('beer_points');
        if( $total_beer_points == 0 )
            return 0;
        return 100 / $total_beer_points * $this->beer_points;
    }

    public function addReturn(){
        if( $this->beer_points == 1 )
            $this->beer_points -= 1;
        if( $this->beer_points > 1 )
            $this->beer_points -= 2;
        
        $this->total_returned++;
        $this->save();
        Log::create([
            'user_id' => $this->id,
            'type' => 'returned',
        ]);
    }

    private function isNewPeriod( $last ){
        $now = Carbon::now();
        if( $now->hour > 12 ){
            return !$last->between(Carbon::now()->hour(12)->minute(0)->second(0), Carbon::tomorrow()->hour(12)->minute(0)->second(0));
        } else {
            return !$last->between(Carbon::yesterday()->hour(12)->minute(0)->second(0), Carbon::now()->hour(12)->minute(0)->second(0));
        }
    }
}
