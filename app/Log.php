<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public $table = 'log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'type'
    ];

    public function scopeNested($query)
    {
        return $query->with('user');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

}
