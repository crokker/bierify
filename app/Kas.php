<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kas extends Model
{
    public $timestamps = false;

    public $table = 'kas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bier'
    ];

    public static function add( $amount ){
        $obj = Self::where('id', 1)->first();
        $obj->bier += $amount;
        $obj->save();
    }

    public static function substract( $amount ){
        $obj = Self::where('id', 1)->first();
        $obj->bier -= $amount;
        $obj->save();
    }

    public static function get(){
        $obj = Self::where('id', 1)->first();
        return $obj->bier;
    }

}
