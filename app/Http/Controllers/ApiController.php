<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Response;
use App\User;
use App\Log;
use App\Kas;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Add a drink to a person
     */
    public function addDrink( $amount = 0 )
    {
        Auth::user()->addDrink((int)$amount);
    }

    /**
     * Add a purchase to a person
     *
     * @return \Illuminate\Http\Response
     */
    public function addPurchase( $amount = 0 )
    {
        Auth::user()->addPurchase((int)$amount);
    }

    /**
     * Add a return
     */
    public function addReturn()
    {
        Auth::user()->addReturn();
    }

    /**
     * Get the user row
     *
     * @return \Illuminate\Http\Response
     */
    public function getUser()
    {
        $user = Auth::user();
        $highest = User::orderBy('beer_points', 'desc')->first();
        $user->beer_quota = $user->getBeerQuota();
        $user->need_purchase = false;
        if( $user->id === $highest->id ){
            $user->need_purchase = true;
        }
        return Response::json($user);
    }

    /**
     * Get all users with beer quota
     *
     * @return \Illuminate\Http\Response
     */
    public function getBeerQuotas()
    {
        $persons = User::orderBy('beer_points', 'desc')->get();
        $array = [];
        $i = 0;
        foreach ($persons as $key => $value) {
            $array[$i]['need_purchase'] = false;
            if( $i == 0 )
                $array[$i]['need_purchase'] = true;
            $array[$i]['beer_quota'] = $value->getBeerQuota();
            $array[$i]['name'] = $value->name;
            $i++;
        }
        return Response::json($array);
    }

    /**
     * Get latest purchaser and returner
     *
     * @return \Illuminate\Http\Response
     */
    public function getLatestLogs()
    {
        $purchaser = Log::nested()->where('type', 'purchased')->orderBy('created_at', 'desc')->first();
        $returner = Log::nested()->where('type', 'returned')->orderBy('created_at', 'desc')->first();
        $the_drunk = User::orderBy('total_drinks', 'desc')->first();
        return Response::json([
                'purchaser' => $purchaser,
                'returner' => $returner,
                'beerking' => $the_drunk,
            ]);
    }

    /**
     * Get amount of beers
     *
     * @return \Illuminate\Http\Response
     */
    public function getStock()
    {
        return Kas::get();
    }

    /**
     * Get buyer
     *
     * @return \Illuminate\Http\Response
     */
    public function getBuyer()
    {
        return Response::json(User::orderBy('beer_points', 'desc')->first());
    }
}
